var clientesObtenidos;
function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}
function procesarClientes() {
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONClientes.value[0].ProductName);
  var tclientes = document.getElementById("tablaClientes");
  var table = document.createElement("table");
  var tbody = document.createElement("tbody");

  table.classList.add("table");
  table.classList.add("table-striped");
  console.log("Hola");
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaContacto = document.createElement("td");
    columnaContacto.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaPais = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }
    columnaPais.appendChild(imgBandera);

    nuevaFila.appendChild(columnaContacto);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);
    tbody.appendChild(nuevaFila);

  }

  table.appendChild(tbody);
  tclientes.appendChild(table);

}
